# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from allauth.socialaccount.models import  SocialAccount


# Create your views here.
def home(request):
    current_user = {'current_user':SocialAccount.objects.get(user=request.user.id)}
    print current_user
    return render(request, 'home/index.html', current_user)
