About

Linkedin OAuth2 using the allauth module. Site stores the linkedin user data in the admin site ('localhost:8000/admin/socialaccount/')

Installation

pip install allauth

Routes
/login

Login button that redirects the user to the linked login page

/home

Displays information for the user currently logged in.


Problems

Not sure how to access the data in the admin site